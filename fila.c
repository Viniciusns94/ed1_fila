#include <stdlib.h>
#include <stdio.h>

#define MAX 8

// --- fila ---
typedef struct {
	int fim;
	int itens[MAX];
}Fila;

void inicia(Fila *F){
	F->fim = -1;
}

void insere(Fila *F, int valor){
	if(F->fim == MAX-1)
		puts("Fila cheia");
	else{
		F->fim++;
		F->itens[F->fim]=valor;
	}
}

int retira(Fila *F){
	int i, x;
	if(F->fim == -1)
		puts("Não existe valor na fila");
	else{
		x = F->itens[0];
		for(i=0; i<F->fim; i++)
			F->itens[i] = F->itens[i+1];
		F->fim--;
		return x;
	}
}

void printTela(Fila *F){
	int i;
	for (i = 0; i < F->fim+1; i++){
		printf("[%d]", F->itens[i]);
	}
	printf("\n");
}

void main(){
	Fila A;
	inicia(&A);
	insere(&A, 5);
	insere(&A, 3);
	insere(&A, 8);
	printf("Fila completa: ");
	printTela(&A);
	printf("Retira 5: ");
	retira(&A);
	printTela(&A);
	printf("Retira 3: ");
	retira(&A);	
	printTela(&A);
}